from time import time
import datetime
from boto.dynamodb2.table import Table

class CreditLine():

	id = ""
	tipo = ""
	cuotas_maximas = ""
	tasa_interes = ""
	fecha_creacion = ""
	fecha_modificacion = ""
	admin_id = ""

	def create(self, admin_id, tipo, cuotas_maximas, tasa_interes):
		id = str(time()).replace('.','')
		table = Table('Credit_Line')
		table.put_item(data={
			'id' : id,
			'tipo' : tipo,
			'cuotas_maximas' : cuotas_maximas,
			'tasa_interes' : tasa_interes,
			'fecha_creacion' : str(datetime.datetime.now()),
			'fecha_modificacion' : "-",
			'admin_id' : admin_id,
			'clients' : set()
		})
		table = Table('Administrator')
		item = table.get_item(id=admin_id)
		if item['credit_lines'] is None:
			item['credit_lines'] = set([id])
		else:
			item['credit_lines'].add(id)
		item.save()

	def load(self, creditline_id):
		table = Table('Credit_Line')
		item = table.get_item(id=creditline_id)
		self.id = item["id"]
		self.tipo = item["tipo"]
		self.cuotas_maximas = item["cuotas_maximas"]
		self.tasa_interes = item["tasa_interes"]
		self.fecha_creacion = item["fecha_creacion"]
		self.fecha_modificacion = item["fecha_modificacion"]
		self.admin_id = item["admin_id"]

	def save(self):
		table = Table('Credit_Line')
		item = table.get_item(id=self.id)
		item['tasa_interes'] = self.tasa_interes
		item['fecha_modificacion'] = self.fecha_modificacion
		item.save()

	def delete(self):
		table = Table('Credit_Line')
		item = table.get_item(id=self.id)
		item.delete()
		table = Table('Administrator')
		item = table.get_item(id=self.admin_id)
		item["credit_lines"].remove(self.id)
		item.save()
