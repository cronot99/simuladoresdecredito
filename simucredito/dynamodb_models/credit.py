from time import time

import boto.sqs
from boto.dynamodb2.table import Table
from boto.sqs.message import RawMessage
from simucredito.dynamodb_models.payment_plan import PaymentPlan

class Credit():

	id = ""
	nombres_cliente = ""
	apellidos_cliente = ""
	tipo_documento_cliente = ""
	numero_documento_cliente = ""
	fecha_nacimiento_cliente = ""
	linea_credito = ""
	valor = ""
	cuotas = ""
	tasa_interes = ""
	fecha_creacion = ""
	nivel_riesgo = ""
	estado = ""
	detalles = []
	
	def create(self, admin_id, client_id, nombres_cliente, apellidos_cliente, tipo_documento_cliente, numero_documento_cliente, fecha_nacimiento_cliente, linea_credito, valor, cuotas, tasa_interes, fecha_creacion):
		id = str(time()).replace('.','')
		table = Table('Credit')
		table.put_item(data={
			'id' : id,
			'nombres_cliente' : nombres_cliente,
			'apellidos_cliente' : apellidos_cliente,
			'tipo_documento_cliente' : tipo_documento_cliente,
			'numero_documento_cliente' : numero_documento_cliente,
			'fecha_nacimiento_cliente' : fecha_nacimiento_cliente,
			'linea_credito' : linea_credito,
			'valor' : valor,
			'cuotas' : cuotas,
			'tasa_interes' : tasa_interes,
			'fecha_creacion' : fecha_creacion,
			'nivel_riesgo' : 'No disponible',
			'estado' : 'En proceso',
			'detalle' : set()
		})
		table = Table('Administrator')
		item = table.get_item(id=admin_id)
		if item['credits'] is None:
			item['credits'] = set([id])
		else:
			item['credits'].add(id)
		item.save()
		table = Table('Client')
		item = table.get_item(id=client_id)
		if item['credits'] is None:
			item['credits'] = set([id])
		else:
			item['credits'].add(id)
		item.save()
		conn = boto.sqs.connect_to_region('us-east-1')
		credits_queue = conn.get_queue('Creditos_Queue')
		message = RawMessage()
		message.set_body(id)
		credits_queue.write(message)

	def load(self, id):
		table = Table('Credit')
		item = table.get_item(id=id)
		self.id = item['id']
		self.nombres_cliente = item['nombres_cliente']
		self.apellidos_cliente = item['apellidos_cliente']
		self.tipo_documento_cliente = item['tipo_documento_cliente']
		self.numero_documento_cliente = item['numero_documento_cliente']
		self.fecha_nacimiento_cliente = item['fecha_nacimiento_cliente']
		self.linea_credito = item['linea_credito']
		self.valor = item['valor']
		self.cuotas = item['cuotas']
		self.tasa_interes = item['tasa_interes']
		self.fecha_creacion = item['fecha_creacion']
		self.nivel_riesgo = item['nivel_riesgo']
		self.estado = item['estado']
		self.detalles = []
		if item['detalle']:
			for each_detalle in item['detalle']:
				detalle = PaymentPlan()
				detalle.load(each_detalle)
				self.detalles.append(detalle)
