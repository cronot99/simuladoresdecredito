from time import time
from boto.dynamodb2.table import Table
from simucredito.dynamodb_models.credit_line import CreditLine
from simucredito.dynamodb_models.credit import Credit

class Administrator():

	id = ""
	nombres = ""
	apellidos = ""
	url = ""
	creditlines = []
	credits = []

	def create(self, tipo_documento, numero_documento, nombres, apellidos, email):
		id = str(time()).replace('.','')
		table = Table('Administrator')
		table.put_item(data={
			'id' : id,
			'nombres' : nombres,
			'apellidos' : apellidos,
			'tipo_documento' : tipo_documento,
			'numero_documento' : numero_documento,
			'email' : email,
			'url' : "http://simuladoresdecredito:8000/simucredito/zonaclientes/" + id + "/client_menu",
			'credit_lines' : set(),
			'credits' : set()
		})
		table = Table('Email_Administrator')
		table.put_item(data={
			'email' : email,
			'id' : id
		})

	def get_id_by_email(self, email):
		table = Table('Email_Administrator')
		return table.get_item(email=email)['id']

	def load(self, id):
		table = Table('Administrator')
		item = table.get_item(id=id)
		self.id = item['id']
		self.nombres = item['nombres']
		self.apellidos = item['apellidos']
		self.url = item['url']
		self.creditlines = []
		if item['credit_lines']:
			for each_credit_line in item['credit_lines']:
				credit_line = CreditLine()
				credit_line.load(each_credit_line)
				self.creditlines.append(credit_line)
		self.credits = []
		if item['credits']:
			for each_credit in item['credits']:
				credit = Credit()
				credit.load(each_credit)
				self.credits.append(credit)

	def first_credit_line(self):
		return self.creditlines[0]
