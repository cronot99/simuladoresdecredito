from boto.dynamodb2.table import Table
from simucredito.dynamodb_models.credit import Credit

class Client():

	nombres = ""
	apellidos = ""
	tipo_documento = ""
	numero_documento = ""
	fecha_nacimiento = ""
	credits = []

	def create(self, admin_id, nombres, apellidos, tipo_documento_id, tipo_documento, numero_documento, fecha_nacimiento):
		id = admin_id + str(tipo_documento_id) + str(numero_documento)
		table = Table('Client')
		table.put_item(data={
			'id' : id,
			'nombres' : nombres,
			'apellidos' : apellidos,
			'tipo_documento' : tipo_documento,
			'numero_documento' : numero_documento,
			'fecha_nacimiento' : fecha_nacimiento,
			'credits' : set()
		})

	def client_exists(self, admin_id, tipo_documento_id, numero_documento):
		table = Table("Client")
		try:
			client_id = admin_id + str(tipo_documento_id) + str(numero_documento)
			table.get_item(id=client_id)
			return True
		except:
			return False

	def load(self, id):
		table = Table('Client')
		item = table.get_item(id=id)
		self.id = item['id']
		self.nombres = item['nombres']
		self.apellidos = item['apellidos']
		self.tipo_documento = item['tipo_documento'],
		self.numero_documento = item['numero_documento'],
		self.fecha_nacimiento = item['fecha_nacimiento'],
		self.credits = []
		if item['credits']:
			for each_credit in item['credits']:
				credit = Credit()
				credit.load(each_credit)
				self.credits.append(credit)