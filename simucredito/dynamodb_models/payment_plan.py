from boto.dynamodb2.table import Table

class PaymentPlan():

	id = ""
	credit_id = ""
	term_id = ""
	gross_payment = ""
	rate_value = ""
	monthly_payment = ""
	balance = ""

	def load(self, id):
		table = Table('Payment_Plan')
		item = table.get_item(id=id)
		self.id = item['id']
		self.credit_id = item['credit_id']
		self.term_id = item['term_id']
		self.gross_payment = item['gross_payment']
		self.rate_value = item['rate_value']
		self.monthly_payment = item['monthly_payment']
		self.balance = item['balance']
