from boto.dynamodb2.table import Table

class CreditType():
	id = ""
	tipo = ""
	cuotas_maximas = 0

	def create(self, id, tipo, cuotas_maximas = 120):
		self.id = id
		self.tipo = tipo
		self.cuotas_maximas = cuotas_maximas

	def load(self, id):
		table = Table('Credit_Line_Type')
		item = table.get_item(id=int(id))
		self.id = item['id']
		self.tipo = item['name']
		self.cuotas_maximas = item['max_loan_terms']

class CreditTypes():
	credit_types = []

	def __init__(self):
		self.credit_types = []
		for each_item in Table("Credit_Line_Type").scan():
			credit_type = CreditType()
			credit_type.create(each_item['id'], each_item['name'], each_item['max_loan_terms'])
			self.credit_types.append(credit_type)

	def get_credit_types(self):
		return self.credit_types

	def first(self):
		return self.credit_types[0]

	def has_types(self):
		if len(self.credit_types) == 0:
			return False
		else:
			return True