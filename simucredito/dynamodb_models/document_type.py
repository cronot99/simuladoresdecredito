from boto.dynamodb2.table import Table

class DocumentType():
	id = ""
	nombre = ""

	def __init__(self, id, nombre):
		self.id = id
		self.nombre = nombre

	def get_document_name(self, doc_id):
		table = Table('Document_Type')
		item = table.get_item(id=int(doc_id))
		return item['name']

class DocumentTypes():
	document_types = []

	def __init__(self):
		self.document_types = []
		for each_item in Table("Document_Type").scan():
			self.document_types.append(DocumentType(each_item['id'], each_item['name']))

	def get_document_types(self):
		return self.document_types
