from django.contrib import admin
from simucredito.models import DocumentType, CreditType, CreditStatus, Administrator, CreditLine, Client, Credit, PaymentPlan 

admin.site.register(DocumentType)
admin.site.register(CreditType)
admin.site.register(CreditStatus)
admin.site.register(Administrator)
admin.site.register(CreditLine)
admin.site.register(Client)
admin.site.register(Credit)
admin.site.register(PaymentPlan)