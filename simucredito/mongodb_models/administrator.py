from time import time
from connection import Connection
from credit_line import CreditLine
from credit import Credit
		
class Administrator():

	id = ""
	nombres = ""
	apellidos = ""
	url = ""
	creditlines = []
	credits = []

	def create(self, tipo_documento, numero_documento, nombres, apellidos, email):
		id = str(time()).replace('.','')
		conn = Connection()
		admin = {
			'_id' : id,
			'nombres' : nombres,
			'apellidos' : apellidos,
			'tipo_documento' : tipo_documento,
			'numero_documento' : numero_documento,
			'email' : email,
			'url' : "https://simuladoresdecredito.herokuapp.com/simucredito/zonaclientes/" + id + "/client_menu",
			'credit_lines' : [],
			'credits' : []
		}
		conn.db.Administrator.insert(admin)

	def get_id_by_email(self, email):
		conn = Connection()
		return conn.db.Administrator.find_one( { "email" : email }, { "_id" : 1 } )['_id']

	def load(self, id):
		conn = Connection()
		document = conn.db.Administrator.find_one( { "_id" : id } )
		self.id = document['_id']
		self.nombres = document['nombres']
		self.apellidos = document['apellidos']
		self.url = document['url']
		self.creditlines = []
		if document['credit_lines']:
			for each_credit_line in document['credit_lines']:
				credit_line = CreditLine()
				credit_line.load(each_credit_line)
				self.creditlines.append(credit_line)
		self.credits = []
		if document['credits']:
			for each_credit in document['credits']:
				credit = Credit()
				credit.load(each_credit)
				self.credits.append(credit)

	def first_credit_line(self):
		return self.creditlines[0]
