from connection import Connection

class DocumentType():
	id = ""
	nombre = ""

	def __init__(self, id, nombre):
		self.id = id
		self.nombre = nombre

	def get_document_name(self, doc_id):
		conn = Connection()
		document = conn.db.Document_Type.find_one( { "_id" : doc_id } )
		return document['name']

class DocumentTypes():
	document_types = []

	def __init__(self):
		self.document_types = []	
		conn = Connection()

		for each_doc in conn.db.Document_Type.find():
			self.document_types.append(DocumentType(each_doc['_id'], each_doc['name']))

	def get_document_types(self):
		return self.document_types
