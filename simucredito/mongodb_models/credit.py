from time import time
from connection import Connection

from boto.sqs.connection import SQSConnection
from boto.sqs.message import RawMessage
from simucredito.mongodb_models.payment_plan import PaymentPlan

class Credit():

	id = ""
	nombres_cliente = ""
	apellidos_cliente = ""
	tipo_documento_cliente = ""
	numero_documento_cliente = ""
	fecha_nacimiento_cliente = ""
	linea_credito = ""
	valor = ""
	cuotas = ""
	tasa_interes = ""
	fecha_creacion = ""
	nivel_riesgo = ""
	estado = ""
	detalles = []
	
	def create(self, admin_id, client_id, nombres_cliente, apellidos_cliente, tipo_documento_cliente, numero_documento_cliente, fecha_nacimiento_cliente, linea_credito, valor, cuotas, tasa_interes, fecha_creacion):
		id = str(time()).replace('.','')
		conn = Connection()
		credit = {
			'_id' : id,
			'nombres_cliente' : nombres_cliente,
			'apellidos_cliente' : apellidos_cliente,
			'tipo_documento_cliente' : tipo_documento_cliente,
			'numero_documento_cliente' : numero_documento_cliente,
			'fecha_nacimiento_cliente' : fecha_nacimiento_cliente,
			'linea_credito' : linea_credito,
			'valor' : valor,
			'cuotas' : cuotas,
			'tasa_interes' : tasa_interes,
			'fecha_creacion' : fecha_creacion,
			'nivel_riesgo' : 'No disponible',
			'estado' : 'En proceso',
			'detalle' : []
		}
		conn.db.Credit.insert(credit)
		credits_list = conn.db.Administrator.find_one( { "_id" : admin_id } )['credits']
		credits_list.append(id)
		conn.db.Administrator.update( { "_id" : admin_id }, { "$set" : { 'credits' : credits_list } } )

		credits_list = conn.db.Client.find_one( { "_id" : client_id } )['credits']
		credits_list.append(id)
		conn.db.Client.update( { "_id" : client_id }, { "$set" : { 'credits' : credits_list } } )

		conn = SQSConnection(is_secure=False, validate_certs=False, debug=1, aws_access_key_id="AKIAI4SUVO4QA3OOCYJA" , aws_secret_access_key="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")
		credits_queue = conn.get_queue('Creditos_Queue')
		message = RawMessage()
		message.set_body(id)
		credits_queue.write(message)

	def load(self, id):
		conn = Connection()
		document = conn.db.Credit.find_one( { "_id" : id } )
		self.id = document['_id']
		self.nombres_cliente = document['nombres_cliente']
		self.apellidos_cliente = document['apellidos_cliente']
		self.tipo_documento_cliente = document['tipo_documento_cliente']
		self.numero_documento_cliente = document['numero_documento_cliente']
		self.fecha_nacimiento_cliente = document['fecha_nacimiento_cliente']
		self.linea_credito = document['linea_credito']
		self.valor = document['valor']
		self.cuotas = document['cuotas']
		self.tasa_interes = document['tasa_interes']
		self.fecha_creacion = document['fecha_creacion']
		self.nivel_riesgo = document['nivel_riesgo']
		self.estado = document['estado']
		self.detalles = []
		if document['detalle']:
			for each_detalle in document['detalle']:
				detalle = PaymentPlan()
				detalle.load(each_detalle)
				self.detalles.append(detalle)
