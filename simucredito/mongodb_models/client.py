from connection import Connection
from simucredito.mongodb_models.credit import Credit

class Client():

	nombres = ""
	apellidos = ""
	tipo_documento = ""
	numero_documento = ""
	fecha_nacimiento = ""
	credits = []

	def create(self, admin_id, nombres, apellidos, tipo_documento_id, tipo_documento, numero_documento, fecha_nacimiento):
		id = admin_id + str(tipo_documento_id) + str(numero_documento)
		conn = Connection()
		client = {
			'_id' : id,
			'nombres' : nombres,
			'apellidos' : apellidos,
			'tipo_documento' : tipo_documento,
			'numero_documento' : numero_documento,
			'fecha_nacimiento' : fecha_nacimiento,
			'credits' : []
		}
		conn.db.Client.insert(client)

	def client_exists(self, admin_id, tipo_documento_id, numero_documento):
		conn = Connection()
		client_id = admin_id + str(tipo_documento_id) + str(numero_documento)
		if conn.db.Client.find_one( { "_id" : client_id } ):
			return True
		else:
			return False

	def load(self, id):
		conn = Connection()
		document = conn.db.Client.find_one( { "_id" : id } )
		self.id = document['_id']
		self.nombres = document['nombres']
		self.apellidos = document['apellidos']
		self.tipo_documento = document['tipo_documento'],
		self.numero_documento = document['numero_documento'],
		self.fecha_nacimiento = document['fecha_nacimiento'],
		self.credits = []
		if document['credits']:
			for each_credit in document['credits']:
				credit = Credit()
				credit.load(each_credit)
				self.credits.append(credit)