from time import time
import datetime
from connection import Connection

class CreditLine():

	id = ""
	tipo = ""
	cuotas_maximas = ""
	tasa_interes = ""
	fecha_creacion = ""
	fecha_modificacion = ""
	admin_id = ""

	def create(self, admin_id, tipo, cuotas_maximas, tasa_interes):
		id = str(time()).replace('.','')
		conn = Connection()
		credit_line = {
			'_id' : id,
			'tipo' : tipo,
			'cuotas_maximas' : cuotas_maximas,
			'tasa_interes' : tasa_interes,
			'fecha_creacion' : str(datetime.datetime.now()),
			'fecha_modificacion' : "-",
			'admin_id' : admin_id,
			'clients' : []
		}
		conn.db.Credit_Line.insert(credit_line)
		credit_lines_list = conn.db.Administrator.find_one( { "_id" : admin_id } )['credit_lines']
		credit_lines_list.append(id)
		conn.db.Administrator.update( { "_id" : admin_id }, { "$set" : { 'credit_lines' : credit_lines_list } } )

	def load(self, creditline_id):
		conn = Connection()
		document = conn.db.Credit_Line.find_one( { "_id" : creditline_id } )
		self.id = document["_id"]
		self.tipo = document["tipo"]
		self.cuotas_maximas = document["cuotas_maximas"]
		self.tasa_interes = document["tasa_interes"]
		self.fecha_creacion = document["fecha_creacion"]
		self.fecha_modificacion = document["fecha_modificacion"]
		self.admin_id = document["admin_id"]

	def save(self):
		conn = Connection()
		conn.db.Credit_Line.update( { "_id" : self.id }, { "$set" : { 'tasa_interes' : self.tasa_interes, 'fecha_modificacion' : self.fecha_modificacion } } )

	def delete(self):
		table = Table('Credit_Line')
		item = table.get_item(id=self.id)
		item.delete()
		table = Table('Administrator')
		item = table.get_item(id=self.admin_id)
		item["credit_lines"].remove(self.id)
		item.save()
