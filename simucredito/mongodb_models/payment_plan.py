from connection import Connection

class PaymentPlan():

	id = ""
	credit_id = ""
	term_id = ""
	gross_payment = ""
	rate_value = ""
	monthly_payment = ""
	balance = ""

	def load(self, id):
		conn = Connection()
		document = conn.db.Payment_Plan.find_one( { "_id" : id } )
		self.id = document['_id']
		self.credit_id = document['creditId']
		self.term_id = document['term_id']
		self.gross_payment = document['gross_payment']
		self.rate_value = document['rate_value']
		self.monthly_payment = document['monthly_payment']
		self.balance = document['balance']
