from connection import Connection

class CreditType():
	id = ""
	tipo = ""
	cuotas_maximas = 0

	def create(self, id, tipo, cuotas_maximas = 120):
		self.id = id
		self.tipo = tipo
		self.cuotas_maximas = cuotas_maximas

	def load(self, id):
		conn = Connection()
		document = conn.db.Credit_Line_Type.find_one( { "_id" : id } )
		self.id = document['_id']
		self.tipo = document['name']
		self.cuotas_maximas = document['max_loan_terms']

class CreditTypes():
	credit_types = []

	def __init__(self):
		self.credit_types = []
		conn = Connection()
		for each_item in conn.db.Credit_Line_Type.find():
			credit_type = CreditType()
			credit_type.create(each_item['_id'], each_item['name'], each_item['max_loan_terms'])
			self.credit_types.append(credit_type)

	def get_credit_types(self):
		return self.credit_types

	def first(self):
		return self.credit_types[0]

	def has_types(self):
		if len(self.credit_types) == 0:
			return False
		else:
			return True