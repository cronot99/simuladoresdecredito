import datetime

from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.contrib import messages 
from django.utils import timezone

from simucredito.mongodb_models.document_type import DocumentTypes, DocumentType
from simucredito.mongodb_models.administrator import Administrator
from simucredito.mongodb_models.credit_type import CreditTypes, CreditType
from simucredito.mongodb_models.credit_line import CreditLine
from simucredito.mongodb_models.client import Client
from simucredito.mongodb_models.credit import Credit
from boto.sqs.connection import SQSConnection
from simucredito.session import Session
from django.core.cache import cache

"""Views for anonymous"""
def index(request):
	return render(request, "simucredito/index.html")

def sqs(request):
	conn = SQSConnection(is_secure=False, validate_certs=False, debug=1, aws_access_key_id="AKIAI4SUVO4QA3OOCYJA" , aws_secret_access_key="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")
	credits_queue = conn.get_queue('Creditos_Queue')
	queue_count = credits_queue.count()
	return render(request, "simucredito/sqs.html", {
		"queue_count" : queue_count
		})

def nuevo_administrador(request):
	return render(request, "simucredito/nuevo_administrador.html", {
		"document_types": DocumentTypes()
		})

def crear_administrador(request):
	administrador = Administrator()
	administrador.create(
		tipo_documento = request.POST["tipo_documento"],
		numero_documento = request.POST["numero_documento"],
		nombres = request.POST["nombres"],
		apellidos = request.POST["apellidos"],
		email = request.POST["email"],
		)
	return HttpResponseRedirect(reverse('simucredito:login_user'))

def login_user(request):
	return render(request, "simucredito/login.html")

def autenticar(request):
	administrador = Administrator()
	try:
		id = administrador.get_id_by_email(request.POST["usuario"])
		print "El id del usuario es " + id
		Session().login(id)
		return HttpResponseRedirect(reverse('simucredito:admin_menu', args=(id,)))
	except:
		return HttpResponseRedirect(reverse('simucredito:login_user'))

def logout_user(request, admin_id):
	Session().logout(admin_id)
	return HttpResponseRedirect(reverse('simucredito:login_user'))

"""Views for administrators"""
def admin_menu(request, admin_id):
	if not Session().verify(admin_id):
		return HttpResponseRedirect(reverse('simucredito:login_user'))
	else:
		administrator = Administrator()
		administrator.load(admin_id)
		return render(request, "simucredito/admin_menu.html", 
			{
			"administrator": administrator,
			})
		
def lineas_credito(request, admin_id):
	if not Session().verify(admin_id):
		return HttpResponseRedirect(reverse('simucredito:login_user'))
	administrator = Administrator()
	administrator.load(admin_id)
	return render(request, "simucredito/lineas_credito.html", 
		{
			"administrator": administrator
		})

def linea_credito(request, admin_id, linea_credito_id):
	if not Session().verify(admin_id):
		return HttpResponseRedirect(reverse('simucredito:login_user'))
	administrator = Administrator()
	administrator.load(admin_id)
	linea_credito = CreditLine()
	linea_credito.load(linea_credito_id)
	return render(request, "simucredito/linea_credito.html", {
		"administrator": administrator,
		"linea_credito": linea_credito
		})

def nueva_linea_credito(request, admin_id):
	if not Session().verify(admin_id):
		return HttpResponseRedirect(reverse('simucredito:login_user'))
	administrator = Administrator()
	administrator.load(admin_id)
	return render(request, "simucredito/nueva_linea_credito.html", {
		"administrator": administrator,
		"credittypes": CreditTypes
		})

def crear_linea_credito(request, admin_id):
	if not Session().verify(admin_id):
		return HttpResponseRedirect(reverse('simucredito:login_user'))
	credit_type = CreditType()
	credit_type.load(request.POST["creditline"])
	creditline = CreditLine()
	creditline.create(admin_id, credit_type.tipo, credit_type.cuotas_maximas, request.POST["tasa_interes"])
	return HttpResponseRedirect(reverse('simucredito:lineas_credito', args=(admin_id,)))

def editar_linea_credito(request, admin_id, linea_credito_id):
	if not Session().verify(admin_id):
		return HttpResponseRedirect(reverse('simucredito:login_user'))
	linea_credito = CreditLine()
	linea_credito.load(linea_credito_id)
	linea_credito.tasa_interes = request.POST["tasa_interes"]
	linea_credito.fecha_modificacion = str(datetime.datetime.now())
	linea_credito.save()
	return HttpResponseRedirect(reverse('simucredito:lineas_credito', args=(admin_id,)))

def eliminar_linea_credito(request, admin_id, linea_credito_id):
	if not Session().verify(admin_id):
		return HttpResponseRedirect(reverse('simucredito:login_user'))
	linea_credito = CreditLine()
	linea_credito.load(linea_credito_id)
	linea_credito.delete()
	return HttpResponseRedirect(reverse('simucredito:lineas_credito', args=(admin_id,)))

def admin_credito(request, admin_id):
	if not Session().verify(admin_id):
		return HttpResponseRedirect(reverse('simucredito:login_user'))
	administrator = Administrator()
	administrator.load(admin_id)
	return render(request, "simucredito/admin_credito.html", 
		{
		"administrator": administrator
		})

def admin_credito_detalles(request, admin_id, credito_id):
	if not Session().verify(admin_id):
		return HttpResponseRedirect(reverse('simucredito:login_user'))
	administrator = Administrator()
	administrator.load(admin_id)
	credito = Credit()
	credito.load(credito_id)
	return render(request, "simucredito/admin_credito_detalles.html", {
		"administrator": administrator,
		"credito": credito,
		"paymentplans": credito.detalles
		})
		
"""Views for clients"""
def client_menu(request, admin_id):
	administrator = Administrator()
	administrator.load(admin_id)
	return render(request, "simucredito/client_menu.html", 
		{
		"administrator": administrator,
		"documenttypes": DocumentTypes(),
		})
	
def nuevo_credito(request, admin_id):
	administrator = Administrator()
	administrator.load(admin_id)
	return render(request, "simucredito/nuevo_credito.html", {
		"administrator": administrator,
		"documenttypes": DocumentTypes()
		})

def crear_credito(request, admin_id):
	cliente = Client()
	client_id = admin_id + str(request.POST["tipo_documento"]) + str(request.POST["numero_documento"])
	credit_line = CreditLine()
	credit_line.load(request.POST["linea_credito"])
	credito = Credit()
	tipo_documento = DocumentType('', '')
	if cliente.client_exists(admin_id, request.POST["tipo_documento"], request.POST["numero_documento"]):
		credito.create(admin_id, client_id, request.POST["nombres"], request.POST["apellidos"], tipo_documento.get_document_name(request.POST["tipo_documento"]), request.POST["numero_documento"], request.POST["fecha_nacimiento"], credit_line.tipo, request.POST["valor"], request.POST["cuotas"], credit_line.tasa_interes, str(datetime.datetime.now()))
	else:
		cliente.create(admin_id, request.POST["nombres"], request.POST["apellidos"], request.POST["tipo_documento"], tipo_documento.get_document_name(request.POST["tipo_documento"]), request.POST["numero_documento"], request.POST["fecha_nacimiento"])
		credito.create(admin_id, client_id, request.POST["nombres"], request.POST["apellidos"], tipo_documento.get_document_name(request.POST["tipo_documento"]), request.POST["numero_documento"], request.POST["fecha_nacimiento"], credit_line.tipo, request.POST["valor"], request.POST["cuotas"], credit_line.tasa_interes, str(datetime.datetime.now()))
	messages.add_message(request, messages.INFO, 'Se ha creado correctamente el credito')
	return HttpResponseRedirect(reverse('simucredito:client_menu', args=(admin_id,)))
	
def buscar_credito(request, admin_id):
	cliente = Client()
	if cliente.client_exists(admin_id, request.POST["tipo_documento"], request.POST["numero_documento"]):
		client_id = admin_id + str(request.POST["tipo_documento"]) + str(request.POST["numero_documento"])
		return HttpResponseRedirect(reverse('simucredito:client_credito', args=(admin_id, client_id)))
	else:
		messages.add_message(request, messages.INFO, 'Cliente no existe / No tiene creditos asociados')
		return HttpResponseRedirect(reverse('simucredito:client_menu', args=(admin_id,)))
		
def client_credito(request, admin_id, client_id):
	administrator = Administrator()
	administrator.load(admin_id)
	cliente = Client()
	cliente.load(client_id)
	return render(request, "simucredito/client_credito.html", 
		{
		"administrator": administrator,
		"cliente": cliente,
		})

def client_credito_detalles(request, admin_id, client_id, credito_id):
	administrator = Administrator()
	administrator.load(admin_id)
	cliente = Client()
	cliente.load(client_id)
	credito = Credit()
	credito.load(credito_id)
	return render(request, "simucredito/client_credito_detalles.html", {
		"administrator": administrator,
		"cliente": cliente,
		"credito": credito,
		"paymentplans": credito.detalles
		})
