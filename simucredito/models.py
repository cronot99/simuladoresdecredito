from datetime import date, datetime

from decimal import Decimal
from django.db import models
from django.utils import timezone
from django.core.validators import MinValueValidator, MaxValueValidator

class DocumentType(models.Model):
    nombre = models.CharField(max_length=50)
    nombre_corto = models.CharField(max_length=3)

    def __str__(self):
        return self.nombre_corto

class CreditType(models.Model):
    tipo = models.CharField(max_length=50)
    cuotas_maximas = models.PositiveIntegerField(validators=[MinValueValidator(6), MaxValueValidator(120)])
    
    def __str__(self):
        return self.tipo

class CreditStatus(models.Model):
	id_estado = models.PositiveIntegerField(validators=[MinValueValidator(0), MaxValueValidator(50)])
	nombre = models.CharField(max_length=50)
	def __str__(self):
		return self.nombre
		
class Administrator(models.Model):
    numero_documento = models.CharField(max_length=20)
    tipo_documento = models.ForeignKey(DocumentType)
    nombres = models.CharField(max_length=200)
    apellidos = models.CharField(max_length=200)
    email = models.EmailField(max_length=50, blank=True, null=True, unique=True)
    url = models.CharField(max_length=500)
	
    def __str__(self):
        return self.numero_documento + " - " + self.nombres + " " + self.apellidos
 
class CreditLine(models.Model):
	administrador = models.ForeignKey(Administrator)
	tipo = models.ForeignKey(CreditType)
	tasa_interes = models.DecimalField(max_digits=3, decimal_places=1)
	fecha_creacion = models.DateField(auto_now_add=True)
	fecha_modificacion = models.DateField(auto_now=True)
	
	def __str__(self):
		return self.tipo.tipo

class Client(models.Model):
    numero_documento = models.CharField(max_length=20)
    tipo_documento = models.ForeignKey(DocumentType)
    nombres = models.CharField(max_length=200)
    apellidos = models.CharField(max_length=200)
    fecha_nacimiento = models.DateTimeField()
    
    def __str__(self):
        return self.numero_documento + " - " + self.nombres + " " + self.apellidos

class Credit(models.Model):
	cliente = models.ForeignKey(Client)
	administrador = models.ForeignKey(Administrator)
	estado = models.ForeignKey(CreditStatus)
	linea_credito = models.ForeignKey(CreditLine)
	tasa_interes = models.DecimalField(max_digits=3, decimal_places=1)
	valor = models.DecimalField(max_digits=20, decimal_places=1)
	cuotas = models.DecimalField(max_digits=20, decimal_places=1)
	nivel_riesgo = models.DecimalField(max_digits=20, decimal_places=1, default=Decimal(-10.0))
	fecha_creacion = models.DateField(auto_now_add=True)
	
	def __str__(self):
		return self.cliente.nombres + " " + self.cliente.apellidos + " - " + self.linea_credito.tipo.tipo

	def get_riesgo(self):
		if self.nivel_riesgo == -10:
			return "No disponible"
		else:
			return self.nivel_riesgo

class PaymentPlan(models.Model):
	cliente = models.ForeignKey(Client)
	credito = models.ForeignKey(Credit)
	cuota = models.PositiveIntegerField(validators=[MinValueValidator(0), MaxValueValidator(120)])
	abono_capital = models.DecimalField(max_digits=10, decimal_places=1)
	interes = models.DecimalField(max_digits=10, decimal_places=1)
	cuota_mensual = models.DecimalField(max_digits=10, decimal_places=1)
	saldo = models.DecimalField(max_digits=10, decimal_places=1)
	
	def __str__(self):
		return self.cliente.nombres + " " + self.cliente.apellidos
