from django.core.cache import cache

class Session():

	def login(self, user_id):
		id = str(user_id)
		logged_list = cache.get('logged')
		if not logged_list:
			logged_list = []
		try:
			logged_list.index(id)
		except ValueError:
			logged_list.append(id)
			cache.set('logged', logged_list)

	def verify(self, user_id):
		id = str(user_id)
		logged_list = cache.get('logged')
		if logged_list:
			try:
				logged_list.index(id)
				return True
			except ValueError:
				return False
		else:
			return False

	def logout(self, user_id):
		id = str(user_id)
		logged_list = cache.get('logged')
		if logged_list:
			try:
				logged_list.remove(id)
				cache.set('logged', logged_list)
				return True
			except ValueError:
				return False

	def clear(self):
		cache.clear()
