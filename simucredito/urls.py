from django.conf.urls import patterns, include, url
from simucredito import views

urlpatterns = patterns('',
	
	url(r'^$', views.index, name='index'),
	url(r'^login_user/$', views.login_user, name='login_user'),
	url(r'^logout_user/(?P<admin_id>\d+)/$', views.logout_user, name='logout_user'),
	url(r'^autenticar/$', views.autenticar, name='autenticar'),
	url(r'^nuevo_administrador/', views.nuevo_administrador, name='nuevo_administrador'),
	url(r'^crear_administrador/', views.crear_administrador, name='crear_administrador'),
    
	url(r'^administradores/(?P<admin_id>\d+)/admin_menu/$', views.admin_menu, name='admin_menu'),
	url(r'^administradores/(?P<admin_id>\d+)/admin_credito/$', views.admin_credito, name='admin_credito'),
	url(r'^administradores/(?P<admin_id>\d+)/admin_credito/admin_credito_detalles/(?P<credito_id>\d+)/$', views.admin_credito_detalles, name='admin_credito_detalles'),
	url(r'^administradores/(?P<admin_id>\d+)/lineas_credito/$', views.lineas_credito, name='lineas_credito'),
    url(r'^administradores/(?P<admin_id>\d+)/nueva_linea_credito/$', views.nueva_linea_credito, name='nueva_linea_credito'),
    url(r'^administradores/(?P<admin_id>\d+)/crear_linea_credito/$', views.crear_linea_credito, name='crear_linea_credito'),
    url(r'^administradores/(?P<admin_id>\d+)/(?P<linea_credito_id>\d+)/linea_credito/$', views.linea_credito, name='linea_credito'),
    url(r'^administradores/(?P<admin_id>\d+)/(?P<linea_credito_id>\d+)/editar_linea_credito/$', views.editar_linea_credito, name='editar_linea_credito'),
	url(r'^administradores/(?P<admin_id>\d+)/(?P<linea_credito_id>\d+)/eliminar_linea_credito/$', views.eliminar_linea_credito, name='eliminar_linea_credito'),
	
	url(r'^zonaclientes/(?P<admin_id>\d+)/nuevo_credito/$', views.nuevo_credito, name='nuevo_credito'),
	url(r'^zonaclientes/(?P<admin_id>\d+)/crear_credito/$', views.crear_credito, name='crear_credito'),
	url(r'^zonaclientes/(?P<admin_id>\d+)/client_menu/$', views.client_menu, name='client_menu'),
	url(r'^zonaclientes/(?P<admin_id>\d+)/client_credito/(?P<client_id>\d+)/$', views.client_credito, name='client_credito'),
	url(r'^zonaclientes/(?P<admin_id>\d+)/buscar_credito/$', views.buscar_credito, name='buscar_credito'),
	url(r'^zonaclientes/(?P<admin_id>\d+)/client_credito/(?P<client_id>\d+)/client_credito_detalles/(?P<credito_id>\d+)/$', views.client_credito_detalles, name='client_credito_detalles'),
)