from django.conf.urls import patterns, include, url
from django.contrib import admin
from simucredito import views

urlpatterns = patterns('',
    url(r'^simucredito/', include('simucredito.urls', namespace="simucredito")),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^hirefire/cba66b54db4b18c2c5e1ef8d391b9a9d3e3d4ba6/info', views.sqs, name='sqs'),
)